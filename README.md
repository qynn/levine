## Levine

40% split ortholinear keyboard based on [Keebio Levinson PCB Rev 3/3.5](https://keeb.io/products/nyquist-keyboard)

<img src="layout/levine.png" alt="levine-L3-L4" width="600"/>
<img src="pics/levine.jpg" alt="levine-rev3" width="600"/>

### QMK Firmware

[QMK Firmware](https://docs.qmk.fm/#/) <br>
[nyquist_rev3](https://github.com/qmk/qmk_firmware/tree/master/keyboards/keebio/nyquist/rev3)

- Install
```
$ git clone https://github.com/qmk/qmk_firmware.git ~/Kbrd/qmk_firmware
```

#### Edit keymap
```
$ cd ~/Kbrd/qmk_firmware/keyboards/keebio/nyquist/keymaps
$ cp default_4x12 qynn
```
NB: created symlink from `[..]/levine/firmware/qynn`


### dfu programmer

- Install (github)

[dfu programmer](https://github.com/dfu-programmer/dfu-programmer)

```
$ git clone https://github.com/dfu-programmer/dfu-programmer.git -->
($ xbps-install automake)
$ cd dfu-programmer -->
$ ./bootstrap.sh -->
$ ./configure -->
$ make -->
$ sudo make install -->
```

- Install (void)
```
:: xbps-install -y dfu-programmer gcc avr-gcc avr-libc elogind hidapi
```

NB: from *sigprof* on QMK discord: `elogind` handles `TAG+="uaccess"` that is set by QMK rules in `/lib/udev/rules.d/73-seat-late.rules`, which triggers permission setting rules

- avr-gcc <= 8
[avr-gcc8](https://github.com/sigprof/void-packages/tree/avr-gcc8/srcpkgs/avr-gcc8)
[void packages](https://github.com/void-linux/void-packages/#quick-start)


NB: from *sigprof* on QMK discord: avr-gcc9 generates bloat that can induce firmware size issues

- udev rules
[udev rules](https://docs.qmk.fm/#/faq_build?id=linux-udev-rules)
```
$ sudo mkdir -p /etc/udev/rules.d
$ sudo cp ~/Kbrd/qmk_firmware/util/udev/50-qmk.rules /etc/udev/rules.d
$ sudo udevadm control --reload-rules
$ sudo udevadm trigger
```

- Flash
[Enter Bootloder](https://docs.qmk.fm/#/newbs_flashing) (`LVL4` + `ESC` + `TAB` on levine)

```
$ cd ~/Kbrd/qmk_firmware
$ make keebio/nyquist/rev3:qynn:dfu
```
NB: if missing dependencies, try QMK cli


### QMK cli

- Install
```
$ python3 -m pip install --user qmk
```
NB: add $HOME/.local/bin to $PATH

- Setup
```
$ qmk setup -H ~/Kbrd/qmk_firmware
$ qmk doctor
```

- Build
```
$ qmk config user.keyboard=keebio/nyquist/rev3
$ qmk config user.keymap=qynn
($ qmk new-keymap)
```

- Compile/Flash
```
$ qmk compile
$ qmk flash
```

/*
Copyright 2017 Danny Nguyen <danny@hexwire.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

// #define USE_I2C

/* Select hand configuration */
/* #define MASTER_RIGHT */

#ifdef RGBLIGHT_ENABLE
  #undef  RGBLIGHT_ANIMATIONS  // saves memory
  #define RGBLIGHT_EFFECT_BREATHING
  #define RGBLIGHT_EFFECT_BREATHE_CENTER 1.0
  #define RGBLIGHT_EFFECT_BREATHE_MAX 190
#endif

#define TAPPING_TERM 300
#define COMBO_COUNT 1
#define COMBO_TERM 200
